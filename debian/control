Source: travispy
Section: python
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@freexian.com>
Build-Depends: debhelper (>= 11), dh-python, python-all, python-setuptools, python3-all, python3-setuptools, python3-sphinx
Standards-Version: 4.1.4
Homepage: https://github.com/menegazzo/travispy
Vcs-Git: https://gitlab.com/kalilinux/packages/travispy.git
Vcs-Browser: https://gitlab.com/kalilinux/packages/travispy
Testsuite: autopkgtest-pkg-python

Package: python-travispy
Architecture: all
Depends: ${python:Depends}, ${misc:Depends}
Suggests: python-travispy-doc
Description: Travis CI API for Python (Python 2)
 This package contains a Python API for Travis CI. It follows the official API
 and is implemented as similar as possible to Ruby implementation.
 .
 This package installs the library for Python 2.

Package: python3-travispy
Architecture: all
Depends: ${python3:Depends}, ${misc:Depends}
Suggests: python-travispy-doc
Description: Travis CI API for Python (Python 3)
 This package contains a Python API for Travis CI. It follows the official API
 and is implemented as similar as possible to Ruby implementation.
 .
 This package installs the library for Python 3.

Package: python-travispy-doc
Architecture: all
Section: doc
Depends: ${sphinxdoc:Depends}, ${misc:Depends}
Description: Travis CI API for Python (common documentation)
 This package contains a Python API for Travis CI. It follows the official API
 and is implemented as similar as possible to Ruby implementation.
 .
 This is the common documentation package.
